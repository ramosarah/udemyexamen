<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Project Worlds || @yield('title')</title>
    <link  rel="stylesheet" href="{{asset('style/css/bootstrap.min.css')}}"/>
    <link  rel="stylesheet" href="{{asset('style/css/bootstrap-theme.min.css')}}"/>    
    <link rel="stylesheet" href="{{asset('style/css/main.css')}}">
    <link  rel="stylesheet" href="{{asset('style/css/font.css')}}">
    <script src="{{asset('style/js/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('style/js/bootstrap.min.js')}}"  type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>

  </head>

  <body>
    <div class="header">
      <div class="row">
        <div class="col-lg-6">
          <span class="logo">Test Your Skill</span>
        </div>
        <div class="col-md-4 col-md-offset-2">
          <span class="pull-right top title1" ><span class="log1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;&nbsp;Hello,</span> <a href="" class="log log1">Héritier</a>&nbsp;|&nbsp;<a href="{{ url('/userlogout') }}" class="log"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>&nbsp;Signout</button></a></span>
        </div>
      </div>
    </div>

    <div class="bg">
      <!-- start navigation menu-->
      <nav class="navbar navbar-default title1">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><b>Netcamp</b></a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active" ><a href="{{ url('/user/home')}}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;Home<span class="sr-only">(current)</span></a></li>
              <li ><a href="{{ url('/user/history')}}"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>&nbsp;History</a></li>
              <li><a href="{{ url('/user/ranking')}}"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span>&nbsp;Ranking</a></li>
              <li class="pull-right"> <a href="{{ url('/userlogout') }}"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;&nbsp;Signout</a></li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter tag ">
              </div>
              <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp;Search</button>
            </form>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
      <!--end navigation menu-->


      {{-- content --}}
      @yield('content')

    <!--Footer start-->
    <div class="row footer">
      <div class="col-md-3 box">
        <a href="" target="_blank">About us</a>
      </div>
      <div class="col-md-3 box">
        <a href="#" data-toggle="modal" data-target="#login">Admin Login</a>
      </div>
      <div class="col-md-3 box">
        <a href="#" data-toggle="modal" data-target="#developers">Developers</a>
      </div>
      <div class="col-md-3 box">
        <a href="{{url('/feedback')}}" target="_blank">Feedback</a>
      </div>
    </div>

    <!-- Modal For Developers-->
    <div class="modal fade title1" id="developers">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" style="font-family:'typo' "><span style="color:orange">Developers</span></h4>
          </div>
        
          <div class="modal-body">
            <p>
              <div class="row">
                <div class="col-md-4">
                  <img src="{{ asset('image/CAM00121.jpg') }}" width=100 height=100 alt="Sunny Prakash Tiwari" class="img-rounded">
                </div>
                <div class="col-md-5">
                  <a href="" style="color:#202020; font-family:'typo' ; font-size:18px" title="Find on Facebook">Yugesh Verma</a>
                  <h4 style="color:#202020; font-family:'typo' ;font-size:16px" class="title1">+91 9165063741</h4>
                  <h4 style="font-family:'typo' ">vermayugesh323@gmail.com</h4>
                  <h4 style="font-family:'typo' ">Chhattishgarh insitute of management & Technology ,bhilai</h4>
                </div>
              </div>
            </p>
          </div>
        </div>
      </div>
    </div>

    <!--Modal for admin login-->
    <div class="modal fade" id="login">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title"><span style="color:orange;font-family:'typo' ">LOGIN</span></h4>
          </div>
          <div class="modal-body title1">
            <div class="row">
              <div class="col-md-3"></div>
                <div class="col-md-6">
                  <form role="form" method="post" action="">
                    <div class="form-group">
                      <input type="text" name="uname" maxlength="20"  placeholder="Admin user id" class="form-control"/> 
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" maxlength="15" placeholder="Password" class="form-control"/>
                    </div>
                    <div class="form-group" align="center">
                      <input type="submit" name="login" value="Login" class="btn btn-primary" />
                    </div>
                  </form>
                </div>
              <div class="col-md-3"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--footer end-->
  </body>
</html>
