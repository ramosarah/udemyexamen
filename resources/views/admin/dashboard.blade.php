@extends('adminlayout.master')


@section('title')
    Dashboard
@endsection


@section('content')

<div class="container"><!--container start-->
    <div class="row">
      <div class="col-md-12">
        <!--home start-->
        <div class="panel">
          <div class="table-responsive">
            <table class="table table-striped title1">
              <tr>
                <td><b>Topic</b></td>
                <td><b>Total question</b></td>
                <td><b>Marks</b></td>
                <td><b>Time limit</b></td>
                <td></td>
              </tr>

              <input type="hidden" {{$increment=1}}>
              @foreach ($quizes as $quiz) 
                <input type="hidden" {{ $restart=0 }}>
                @foreach ($scores as $score)
                  @if(Session::get('admin')->email == $score->email && $quiz->topic == $score->topic)
                    <input type="hidden" {{ $restart++ }}>
                  @endif
                @endforeach


                @if ($restart == 1)
                  <tr>
                    <td>{{$increment}}</td>
                    <td>{{ $quiz->topic }}&nbsp;<span title="This quiz is already solve by you" class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>                    
                    <td>{{ $quiz->totalquestions }}</td>
                    <td>{{ $quiz->mark * $quiz->totalquestions }}</td>
                    <td>{{ $quiz->timelimit}}&nbsp;min</td>
                    <td><b><a href="/admin/respond/{{$quiz->topic}}" class="pull-right btn sub1" style="margin:0px;background:red"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Restart</b></span></a></b></td>
                  </tr>
                  <input type="hidden" {{$increment++}}>

                @else
                <tr>
                  <td>{{$increment}}</td>
                  <td>{{ $quiz->topic }}</td>
                  <td>{{ $quiz->totalquestions }}</td>
                  <td>{{ $quiz->mark * $quiz->totalquestions }}</td>
                  <td>{{ $quiz->timelimit}}&nbsp;min</td>
                  <td><b><a href="/admin/respond/{{$quiz->topic}}" class="pull-right btn sub1" style="margin:0px;background:#99cc32"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Start</b></span></a></b></td>
                </tr>
                @endif
                  
                
                <input type="hidden" {{$increment++}}>
                
              @endforeach




              {{-- <tr>
                <td>2</td>
                <td>Linux:startup</td>
                <td>5</td>
                <td>10</td>
                <td>10&nbsp;min</td>
                <td><b><a href="" class="pull-right btn sub1" style="margin:0px;background:#99cc32"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Start</b></span></a></b></td>
              </tr>
              <tr>
                <td>3</td>
                <td>Networking</td>
                <td>2</td>
                <td>4</td>
                <td>5&nbsp;min</td>
                <td><b><a href="" class="pull-right btn sub1" style="margin:0px;background:#99cc32"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Start</b></span></a></b></td>
              </tr>
              <tr>
                <td>4</td>
                <td>C++ Coding</td>
                <td>2</td>
                <td>4</td>
                <td>5&nbsp;min</td>
                <td><b><a href="" class="pull-right btn sub1" style="margin:0px;background:#99cc32"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Start</b></span></a></b></td>
              </tr>
              <tr>
                <td>5</td>
                <td>Php Coding</td>
                <td>2</td>
                <td>4</td>
                <td>5&nbsp;min</td>
                <td><b><a href="" class="pull-right btn sub1" style="margin:0px;background:#99cc32"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Start</b></span></a></b></td>
              </tr>
              <tr>
                <td>6</td><td>Linux : File Managment</td><td>2</td><td>4</td><td>5&nbsp;min</td>
                <td><b><a href="" class="pull-right btn sub1" style="margin:0px;background:#99cc32"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>&nbsp;<span class="title1"><b>Start</b></span></a></b></td>
              </tr> --}}
            </table>
        </div>
      </div>
    </div>
  </div>
</div><!--container closed-->

@endsection