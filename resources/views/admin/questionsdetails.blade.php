@extends('adminlayout.master')


@section('title')
    Questions details
@endsection


@section('content')
  <!--container start-->

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <span class="title1" style="margin-left:40%;font-size:30px;"><b>Enter Question Details</b></span><br /><br />
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <form class="form-horizontal title1" name="form" action="{{ url('/admin/savequestion')}}"  method="POST">
              @csrf
                <b>Question number {{$quiz->num+1}}:</b><br/>

                <div class="form-group">
                  <label class="col-md-12 control-label" for="question "></label>  
                  <div class="col-md-12">
                  <textarea rows="3" cols="5" name="question" class="form-control" placeholder="Write question number 1 here..."></textarea>  
                  </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-12 control-label" for="a"></label>  
                  <div class="col-md-12">
                  <input id="optiona" name="optiona" placeholder="Enter option a" class="form-control input-md" type="text">
                    
                  </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-12 control-label" for="a"></label>  
                  <div class="col-md-12">
                  <input id="optionb" name="optionb" placeholder="Enter option b" class="form-control input-md" type="text">
                    
                  </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-12 control-label" for="c"></label>  
                  <div class="col-md-12">
                  <input id="optionc" name="optionc" placeholder="Enter option c" class="form-control input-md" type="text">
                    
                  </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-12 control-label" for="d"></label>  
                  <div class="col-md-12">
                  <input id="optiond" name="optiond" placeholder="Enter option d" class="form-control input-md" type="text">
                    
                  </div>
                </div>
                <br />
                <b>Correct answer</b>:<br />
                <select id="correct" name="correct" placeholder="Choose correct answer " class="form-control input-md" required>
                  <option value="">Select answer for question  {{$quiz->num+1}}</option>
                  <option value="a">option a</option>
                  <option value="b">option b</option>
                  <option value="c">option c</option>
                  <option value="d">option d</option> 
                </select><br /><br />
                <div class="form-group">
                  <label class="col-md-12 control-label" for=""></label>
                  <div class="col-md-12"> 
                    <input  type="submit" style="margin-left:45%" class="btn btn-primary" value="Submit" class="btn btn-primary"/>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end container -->

@endsection