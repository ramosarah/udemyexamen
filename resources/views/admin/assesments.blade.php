@extends('adminlayout.master')


@section('title')
    Assessment
@endsection


@section('content')

      <!--container start-->
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="panel" style="margin:5%">
              <b>Question &nbsp;{{Session::get("num")}}&nbsp;::
              <br />{{ $question->question }}</b><br /><br />
                <form action="{{ url('/admin/saveanswer') }}" method="POST"  class="form-horizontal">
                  @csrf
                  <input type="radio" value="{{ $question->b }}" name="ans">&nbsp;{{ $question->b }}<br /><br />
                  <input type="radio" value="{{ $question->c }}" name="ans">&nbsp;{{ $question->c }}<br /><br />
                  <input type="radio" value="{{ $question->d }}" name="ans">&nbsp;{{ $question->d }}<br /><br /><br />
                  <input type="radio" value="{{ $question->a }}" name="ans">&nbsp;{{ $question->a }}<br /><br />
                  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>&nbsp;Submit</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection