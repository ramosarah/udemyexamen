<!DOCTYPE html>
<html>
  
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Project Worlds || TEST YOUR SKILL </title>
    <link  rel="stylesheet" href="{{asset('style/css/bootstrap.min.css')}}"/>
    <link  rel="stylesheet" href="{{asset('style/css/bootstrap-theme.min.css')}}"/>    
    <link rel="stylesheet" href="{{asset('style/css/main.css')}}">
    <link  rel="stylesheet" href="{{asset('style/css/font.css')}}">
    <script src="{{asset('style/js/jquery.js')}}" type="text/javascript"></script>

    <script src="{{asset('style/js/bootstrap.min.js')}}"  type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <script>
      function validateForm() {var y = document.forms["form"]["name"].value;	var letters = /^[A-Za-z]+$/;if (y == null || y == "") {alert("Name must be filled out.");return false;}var z =document.forms["form"]["college"].value;if (z == null || z == "") {alert("college must be filled out.");return false;}var x = document.forms["form"]["email"].value;var atpos = x.indexOf("@");
      var dotpos = x.lastIndexOf(".");if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {alert("Not a valid e-mail address.");return false;}var a = document.forms["form"]["password"].value;if(a == null || a == ""){alert("Password must be filled out");return false;}if(a.length<5 || a.length>25){alert("Passwords must be 5 to 25 characters long.");return false;}
      var b = document.forms["form"]["cpassword"].value;if (a!=b){alert("Passwords must match.");return false;}}
    </script>
  </head>

  <body>

    <!--header start-->
    <div class="row header">
      <div class="col-lg-6">
        <span class="logo">Test Your Skill</span>
      </div>
      <div class="col-md-2">
      </div>
      <div class="col-md-4">
        <a href="#" class="pull-right sub1 btn title3" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>&nbsp;Signin</a>&nbsp;
        <a href="" class="pull-right btn sub1 title3"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;Home</a>&nbsp;
      </div>
    </div>

    <!--sign in modal start-->
    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content title1">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title title1"><span style="color:orange">Log In</span></h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" action="login.php?q=index.php" method="POST">
              <fieldset>
              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-3 control-label" for="email"></label>  
                <div class="col-md-6">
                  <input id="email" name="email" placeholder="Enter your email-id" class="form-control input-md" type="email">
                </div>
              </div>

              <!-- Password input-->
              <div class="form-group">
                <label class="col-md-3 control-label" for="password"></label>
                <div class="col-md-6">
                  <input id="password" name="password" placeholder="Enter your Password" class="form-control input-md" type="password">
                </div>
              </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Log in</button>
              </fieldset>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!--sign in modal closed-->

    <!--header end-->

    <!-- start container -->
    <div>  
      <div class="bg1">
        <div class="row">
          <div class="col-md-3"></div>
            <div class="col-md-6 panel" style="background-image:url(image/bg1.jpg); min-height:430px;">
              <h2 align="center" style="font-family:'typo'; color:#000066">FEEDBACK/REPORT A PROBLEM</h2>
              <div style="font-size:14px">
              
              You can send us your feedback through e-mail on the following e-mail id:<br />
              <div class="row">
                <div class="col-md-1"></div>
                  <div class="col-md-10">
                  <a href="mailto:serbermz2020@gmail.com" style="color:#000000">serbermz2020@gmail</a><br /><br />
                  </div>
                <div class="col-md-1"></div>
              </div>

              <p>Or you can directly submit your feedback by filling the enteries below:-</p>

                  <form role="form"  method="post" action="/">
                    
                    <div class="row">
                      <div class="col-md-3"><b>Name:</b><br /><br /><br /><b>Subject:</b></div>
                      <div class="col-md-9">
                        <!-- Text input-->
                        <div class="form-group">
                          <input id="name" name="name" placeholder="Enter your name" class="form-control input-md" type="text"><br />    
                          <input id="name" name="subject" placeholder="Enter subject" class="form-control input-md" type="text">    

                        </div>
                      </div>
                    </div><!--End of row-->

                    <div class="row">
                      <div class="col-md-3"><b>E-Mail address:</b></div>
                      <div class="col-md-9">
                      <!-- Text input-->
                        <div class="form-group">
                          <input id="email" name="email" placeholder="Enter your email-id" class="form-control input-md" type="email">    
                        </div>
                      </div>
                    </div><!--End of row-->

                    <div class="form-group"> 
                      <textarea rows="5" cols="8" name="feedback" class="form-control" placeholder="Write feedback here..."></textarea>
                    </div>

                    <div class="form-group" align="center">
                      <input type="submit" name="submit" value="Submit" class="btn btn-primary" />
                    </div>
                  </form>
              </div><!--col-md-6 end-->
              <div class="col-md-3"></div>
            </div>
        </div>
      </div>
    </div><!--container end-->


    <!--Footer start-->
    <div class="row footer">
      <div class="col-md-3 box">
        <a href="https://www.youtube.com/channel/UCsFgC9ggwrmYR2XqEHXpbNg" target="_blank">About us</a>
      </div>

      <div class="col-md-3 box">
        <a href="#" data-toggle="modal" data-target="#login">Admin Login</a>
      </div>

      <div class="col-md-3 box">
        <a href="#" data-toggle="modal" data-target="#developers">Developer</a>
      </div>

      <div class="col-md-3 box">
        <a href="feedback.php" target="_blank">Feedback</a>
      </div>
    </div>

    <!-- Modal For Developers-->
    <div class="modal fade title1" id="developers">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" style="font-family:'typo' "><span style="color:orange">Developer</span></h4>
          </div>
        
          <div class="modal-body">
            <p>
            <div class="row">

              <div class="col-md-4">
                <img src="image/CAM00121.jpg" width=100 height=100 alt="Lyndon Rife Bermoy" class="img-rounded">
              </div>

              <div class="col-md-5">
                <a href="https://www.facebook.com/donnieboiii" style="color:#202020; font-family:'typo' ; font-size:18px" title="Find on Facebook">Lyndon Rife Bermoy</a>
                <h4 style="color:#202020; font-family:'typo' ;font-size:16px" class="title1">+639079373999</h4>
                <h4 style="font-family:'typo' ">serbermz2020@gmail.com</h4>
                <h4 style="font-family:'typo' ">Philippine Science High School-Caraga Region Campus</h4>
              </div>

            </div>
            </p>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--Modal for admin login-->
    <div class="modal fade" id="login">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title"><span style="color:orange;font-family:'typo' ">LOGIN</span></h4>
          </div>

          <div class="modal-body title1">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <form role="form" method="post" action="admin.php?q=index.php">
                  <div class="form-group">
                    <input type="text" name="uname" maxlength="20"  placeholder="Admin user id" class="form-control"/> 
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" maxlength="15" placeholder="Password" class="form-control"/>
                  </div>
                  <div class="form-group" align="center">
                    <input type="submit" name="login" value="Login" class="btn btn-primary" />
                  </div>
                </form>
              </div>
              <div class="col-md-3"></div>
            </div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!--footer end-->

  </body>
</html>
