<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|


Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/feedback', [UserController::class, 'feedback']);
Route::get('/feedbacksuccess', [UserController::class, 'feedbacksuccess']);

Route::get('/', [LoginController::class, 'login']);
Route::post('/adminaccess', [LoginController::class, 'adminaccess']);
Route::get('/adminlogout', [LoginController::class, 'adminlogout']);
Route::post('/usersignup', [LoginController::class, 'usersignup']);
Route::get('/userlogout', [LoginController::class, 'userlogout']);
Route::post('/useraccess', [LoginController::class, 'useraccess']);

Route::get('/admin/dashboard', [AdminController::class, 'dashboard']);
Route::get('/admin/users', [AdminController::class, 'users']);
Route::get('/admin/ranking', [AdminController::class, 'ranking']);
Route::get('/admin/feedback', [AdminController::class, 'feedback']);
Route::get('/admin/viewfeedback', [AdminController::class, 'viewfeedback']);
Route::get('/admin/quizdetails', [AdminController::class, 'quizdetails']);
Route::get('/admin/questionsdetails', [AdminController::class, 'questionsdetails']);
Route::get('/admin/removequiz', [AdminController::class, 'removequiz']);
Route::post('/admin/savequiz', [AdminController::class, 'savequiz']);
Route::post('/admin/savequestion', [AdminController::class, 'savequestion']);
Route::get('/admin/respond/{topic}', [AdminController::class, 'respondquestion']);
Route::post('/admin/saveanswer', [AdminController::class, 'saveanswer']);
Route::get('/admin/response1', [AdminController::class, 'response1']);
Route::get('/admin/results', [AdminController::class, 'results']);
Route::get('/admin/deletedeveloper/{email}', [AdminController::class, 'deletedeveloper']);

Route::get('/admin/assesments', [AdminController::class, 'assesments']);
 
Route::get('/user/home', [UserController::class, 'home']);
Route::get('/user/history', [UserController::class, 'history']);
Route::get('/user/ranking', [UserController::class, 'ranking']);
Route::get('/user/exam', [UserController::class, 'exam']);
Route::get('/user/respond/{topic}', [UserController::class, 'respondquestion']);
Route::get('/user/assesments', [UserController::class, 'assesments']);
Route::post('/user/saveanswer', [UserController::class, 'saveanswer']);
Route::get('/user/response1', [UserController::class, 'response1']);
Route::get('/user/results', [UserController::class, 'results']);
