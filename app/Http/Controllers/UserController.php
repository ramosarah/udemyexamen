<?php

namespace App\Http\Controllers;

use App\Models\Developer;
use App\Models\Question;
use App\Models\Quiz;
use App\Models\Score;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function home() {
        $scores = Score::all();
        $quizes = Quiz::get();
        return view('user.home', compact('quizes', 'scores'));
    }

    public function history() {
        $scores = Score::where('email', Session::get('developer')->email)->get();
        return view('user.history')->with('score', $scores);
    }

    public function ranking() {
        $developers = Developer::orderBy('score', 'desc')->get();
        return view('user.ranking')->with('developers', $developers);
    }

    public function exam() {
        return view('user.exam');
    }


    public function respondquestion($topic) {
        if(!Session::get('num') && !Session::get('quiz')) {
            Session::put('num', 1);            
            $quiz = Quiz::where('topic', $topic)->first();
            Session::put('quiz', $quiz);
        }

        
        return redirect('/user/assesments');
    }

    

    public function assesments() {
        //$question = Question::where('topic', Session::get('quiz')->topic)->first();
        //$num = $question->numquestion;
        $question = Question::where('topic', Session::get('quiz')->topic)->where('numquestion', Session::get('num'))->first();
        //dd($question);
        Session::put('question', $question);
        return view('user.assesments', compact('question'));
    }



    public function saveanswer(Request $request) {
        $score = Score::where('email', Session::get('developer')->email)->where('topic', Session::get('quiz')->topic)->first();
        $developer = Developer::where('email', Session::get('developer')->email)->first();

        if($request->input('ans')) {
            if($request->input('ans') == Session::get('question')->correct) {
                if(!Session::get('score')) {
                    Session::put('score', Session::get('quiz')->mark);
                } else {
                    $sc = Session::get('score') + Session::get('quiz')->mark;
                    Session::forget('score');
                    Session::put('score', $sc);
                }
            }

            if(!Session::get('solved')) {
                Session::put('solved', 1);
            } else {
                $solved = Session::get('solved') + 1;
                Session::forget('solved');
                Session::put('solved', $solved);
            }
        }

        if(!$score && Session::get('num') == Session::get('quiz')->totalquestions) {
            $score = new Score();
            $score->topic = Session::get('quiz')->topic;
            $score->email = Session::get('developer')->email;

            if(Session::get('score')) {
                $score->score = Session::get('score');
                $developer->score = $developer->score + Session::get('score');
                $developer->update();
            } else {
                $score->score = 0;
            }

            $score->mark = Session::get('quiz')->mark;
            $score->numquestion = Session::get('quiz')->totalquestions;
            
            if(Session::get('solved')) {
                $score->solved = Session::get('solved');
            } else {
                $score->solved = 0;
            }

            $score->save();

        } elseif($score && Session::get('num') == Session::get('quiz')->totalquestions) {
            if(Session::get('score')) {
                $developer->score = ($developer->score - $score->score) + Session::get('score');
                $score->score = Session::get('score');
                $developer->update();
            } else {
                $score->score = 0;
            }

            if(Session::get('solved')) {
                $score->solved = Session::get('solved');
            } else {
                $score->solved = 0;
            }

            $score->update();
        }


        return redirect('/user/response1');
    }


    public function response1() {
        $num = Session::get('num') + 1;
        Session::forget('num');
        Session::put('num', $num); 

        if($num <= Session::get('quiz')->totalquestions) {
            return redirect('/user/assesments');
        } else {
            Session::forget('num');
            Session::forget('score');
            Session::forget('solved');
            //Session::flush(); ??
            return redirect('/user/results');
        }
        
    }

    public function results() {
        if(Session::get('quiz')) {
            $score = Score::where('topic', Session::get('quiz')->topic)->where('email', Session::get('developer')->email)->first();

            Session::forget('quiz');
            return view('user.results')->with('score', $score);
        } else {
            return redirect('/user/home');
        }
    }

    public function feedback() {
        return view('feedback.feedback');
    }

    public function feedbacksuccess() {
        return view('feedback.feedbacksuccess');
    }


}
