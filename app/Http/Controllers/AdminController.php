<?php

namespace App\Http\Controllers;

use App\Models\Developer;
use App\Models\Question;
use App\Models\Quiz;
use App\Models\Score;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function dashboard() {
        $scores = Score::all();
        $quizes = Quiz::get();
        return view('admin.dashboard', compact('quizes', 'scores'));
    }

    public function users() {
        $developers = Developer::get();
        return view('admin.users', compact('developers'));    
    } 

    public function ranking() {
        $developers = Developer::orderBy('score', 'desc')->get();
        return view('user.ranking')->with('developers', $developers);
    }

    public function feedback() {
        return view('admin.feedback');
    }

    public function viewfeedback() {
        return view('admin.viewfeedback');
    }

    public function deletedeveloper($email) {
        $developer = Developer::where('email', $email)->first();
        $scores = Score::where('email', $email)->get();

        if($scores) {
            foreach($scores as $score) {
                $score->delete();
            }
        }
        
        $developer->delete();
        
        return back()->with('status', 'Le compte est suprimé');

    }

    public function quizdetails() {
        return view('admin.quizdetails');
    }

    public function questionsdetails() {
        $quiz = Quiz::where('topic', Session::get('quiz')->topic)->first();
        if ($quiz->num < $quiz->totalquestions) {
            return view('admin.questionsdetails', compact('quiz'));
        } else {
            Session::forget('quiz');
            return redirect('/admin/dashboard');
        }
        
    }

    public function savequestion(Request $request) {
        $correct = '';

        if ($request->input('correct') == "a") {
            $correct = $request->input('optiona');
        } elseif ($request->input('correct') == "b") {
            $correct = $request->input('optionb');
        } elseif ($request->input('correct') == "c") {
            $correct = $request->input('optionc');
        } else {
            $correct = $request->input('optiond');
        }

        $question = new Question();
        $question->topic = Session::get('quiz')->topic;
        $question->question = $request->question;
        $question->a = $request->optiona;
        $question->b = $request->optionb;
        $question->c = $request->optionc;
        $question->d = $request->optiond;
        $question->correct = $correct; //ici ya un truc qui va pas...

        $quiz = Quiz::where('topic', Session::get('quiz')->topic)->first();
        $quiz->num = $quiz->num + 1;
        $quiz->update();

        $question->numquestion = $quiz->num;
        $question->save();

        return redirect('/admin/questionsdetails');




    }

    public function respondquestion($topic) {
        if(!Session::get('num') && !Session::get('quiz')) {
            Session::put('num', 1);            
            $quiz = Quiz::where('topic', $topic)->first();
            Session::put('quiz', $quiz);
        }
        $question = Question::where('topic', $topic)->first();
        $num = $question->numquestion;
        //dd($questionNum);
        //$num = Question::where('numquestion', Session::get('num'))->first();
        //$question = Question::where('topic', Session::get('quiz')->topic)->where('numquestion', Session::get('num'))->first();
        Session::put('question', $question);
        Session::put('num', $num);
        return view('admin.assesments', compact(['question', 'topic', 'num']));

/*         return redirect('/admin/assesments');
 */    }

    public function assesments() {
        $question = Question::where('topic', Session::get('quiz')->topic)->first();
        $num = $question->numquestion;
        //$question = Question::where('topic', Session::get('quiz')->topic)->where('numquestion', Session::get('num'))->first();
        //dd($question);
        Session::put('question', $question);
        return view('admin.assesments', compact('question'));
    }



    public function saveanswer(Request $request) {
        $score = Score::where('email', Session::get('admins')->email)->where('topic', Session::get('quiz')->topic)->first();
 
        if($request->input('ans')) {
            if($request->input('ans') == Session::get('question')->correct) {
                if(!Session::get('score')) {
                    Session::put('score', Session::get('quiz')->mark);
                } else {
                    $sc = Session::get('score') + Session::get('quiz')->mark;
                    Session::forget('score');
                    Session::put('score', $sc);
                }
            }

            if(!Session::get('solved')) {
                Session::put('solved', 1);
            } else {
                $solved = Session::get('solved') + 1;
                Session::forget('solved');
                Session::put('solved', $solved);
            }
        }

        if(!$score && Session::get('num') == Session::get('quiz')->totalquestions) {
            $score = new Score();
            $score->topic = Session::get('quiz')->topic;
            $score->email = Session::get('admin')->email;

            if(Session::get('score')) {
                $score->score = Session::get('score');
            } else {
                $score->score = 0;
            }

            $score->mark = Session::get('quiz')->mark;
            $score->numquestion = Session::get('quiz')->totalquestions;
            
            if(Session::get('solved')) {
                $score->solved = Session::get('solved');
            } else {
                $score->solved = 0;
            }

            $score->save();

        } elseif($score && Session::get('num') == Session::get('quiz')->totalquestions) {
            if(Session::get('score')) {
                $score->score = Session::get('score');
            } else {
                $score->score = 0;
            }

            if(Session::get('solved')) {
                $score->solved = Session::get('solved');
            } else {
                $score->solved = 0;
            }

            $score->update();
        }


        return redirect('/admin/response1');
    }

    public function response1() {
        $num = Session::get('num') + 1;
        Session::forget('num');
        Session::put('num', $num); 

        if($num <= Session::get('quiz')->totalquestions) {
            return redirect('/admin/assesments');
        } else {
            Session::forget('num');
            Session::forget('score');
            Session::forget('solved');

            //Session::flush(); ??
            return redirect('/admin/results');
        }
        
    }


    public function results() {
        $score = Score::where('topic', Session::get('quiz')->topic)->where('email', Session::get('admin')->email)->first();

        Session::forget('quiz');
        return view('admin.results');
    }

    public function removequiz() {
        return view('admin.removequiz')->with('score');
    }

    public function savequiz(Request $request) {
        $quiz = new Quiz();
        $quiz->topic = $request->topic;
        $quiz->totalquestions = $request->totalquestions;
        $quiz->mark = $request->mark;
        $quiz->timelimit = $request->timelimit;
        $quiz->description = $request->description;
        $quiz->num = 0;
        $quiz->save();

        $quiz = Quiz::where('topic', $request->topic)->first();
        Session::put('quiz', $quiz);

        return redirect('/admin/questionsdetails');

    }

    
}
