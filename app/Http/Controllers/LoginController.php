<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Developer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function login() {
        return view('login.login');
    }

    public function adminaccess(Request $request) {
        $admin = Admin::where('email', $request->email)->first();
        if ($admin) {
            if($admin->password == $request->password) {
                Session::put('admin', $admin);
                return redirect('/admin/dashboard');
            }
            else {
                return back();
            }
        } else {
            return back();
        }
        
    }


    public function adminlogout() {
        Session::forget('admin');

        return redirect('/');
    }



    public function usersignup(Request $request) {
        $developper = new Developer();
        $developper->name = $request->name;
        $developper->gender = $request->gender;
        $developper->college = $request->college;
        $developper->email = $request->email;
        $developper->phone = $request->phone;
        $developper->password = $request->password;
        $developper->score = 0;

        $developper->save();
        Session::put('developper', $developper);

        return redirect('/user/home');
        
    }


    public function useraccess(Request $request) {
        $developper = Developer::where('email', $request->email)->first();
        if ($developper) {
            if($developper->password == $request->password) {
                Session::put('developper', $developper);
                return redirect('/user/home');
            }
            else {
                return back();
            }
        } else {
            return back();
        }
    }

    public function userlogout() {
        Session::forget('developper');
        return view('.login.login');
    }
}
